<?php
/* $Id: index.php,v 0.0.0.1 22/04/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
 * www.kimera-lab.com Index Script
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

include 'includes.php';
$time_start = getmicrotime(); // Timing rendering page

?>
<!--
$Id: index.php,v 0.0.0.1 22/04/2006 02:02:07 mdb Exp $
$Author: mdb $

www.kimera-lab.com Index Script

Copyright Kimera Team (c) 2006

You may not reproduce it elsewhere without the prior written permission of the author.
However, feel free to study the code and use techniques you learn from it elsewhere.
-->

<?php print "<?xml version='1.0' encoding='iso-8859-1' ?>"; ?>

<!DOCTYPE html
         PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'
         'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>

<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>

         <head>
                  <title><?php print $headache['title']; ?></title>

<?php foreach($headache as $metaname => $metacontent) {?>
				  <meta name='<?php print $metaname; ?>' content='<?php print $metacontent; ?>' />
<?php } ?>
<?php foreach($headmeta as $metahttp => $metacontent) {?> 
				  <meta http-equiv='<?php print $metahttp; ?>' content='<?php print $metacontent; ?>' />
<?php } ?>
<?php foreach($headlinks as $rel => $hreftype) { $divide = explode("###", $hreftype); ?> 
				  <link rel="<?php print $rel; ?>" href="<?php print $divide[0]; ?>" type="<?php print $divide[1]; ?>" />
<?php flush($divide); } ?>
         </head>

         <body onload="<?php print $onload; ?>">
<?php 
        kimera();
        $time_end = getmicrotime();
        $ktime = $time_end - $time_start;
?>
		 <p class="footer">
				Page rendered in: <?php print substr($ktime, 0, 6); ?> seconds<br />
				@2006 Kimera Team
		 </p>
         </body>

</html>