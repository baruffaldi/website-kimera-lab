<?php
/* $Id: content.php,v 0.0.0.1 22/04/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
 * www.kimera-lab.com Configuration File
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

$headache = Array
(
   'title' => "Kimera Lab :: Stiamo arrivando!!",
   'subject' => "Web Development Team",
   'metadata' => "Kimera Lab",
   'author' => "Filippo Baruffaldi, Patrizio Belvedere",
   'publisher' => "www.kimera-lab.com",
   'date' => "20Apr2006",
   'form' => "xhtml"
);

$headmeta = Array
(
   'Keywords' => "",
   'Description' => "Web Development Team",
   'Content-Type' => "text/xhtml; charset=iso-8859-1"
);

$headlinks = Array
(
   'stylesheet' => "css/style.css###text/css",
);

$onload = "window.defaultStatus='Kimera Lab :: Stiamo arrivando!!'";

?>