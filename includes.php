<?php
/* $Id: vars.php,v 0.0.0.1 22/04/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
 * www.kimera-lab.com Include Scripts File
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

include 'global.php';
include 'content.php';
include 'kimera.php';

?>