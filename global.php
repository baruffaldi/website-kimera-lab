<?php
/* $Id: vars.php,v 0.0.0.1 22/04/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
 * www.kimera-lab.com Global Configuration File
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

/*
 *----------------------------------------------
 * ====> DO NOT EDIT BELOW <====
 *----------------------------------------------
*/

/* Catch the GET and POST variables
*/
foreach($_GET as $key => $value) {
 	$$key = $value;
}
foreach($_POST as $key => $value) {
 	$$key = $value;
}

/* Creating Global Variables
*/
$sitepath = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REDIRECT_URL'];

/*
###########
## Languages ##
##########
*/

/* Check if had been choosed a language
*/ 
if (empty($klang)) $klang = "Italiano";

/* Catch the available languages
*/
$dh=opendir($sitepath . "languages");

while ($file=readdir($dh)) {
		$filep = explode(".", $file);
		$filename = $filep[0];
		$filext = array_pop($filep);
		if ($filext == "kml" && $filename == $klang) include "$sitepath$file";
}

/*
############
## Enviroments ##
###########
*/
function getmicrotime()
{
         list($usec, $sec) = explode(" ",microtime());
         return ((float)$usec + (float)$sec);
}

?>